### Introduction
This library presents a different way to handle errors in code. Typical
applications will just `throw` Exceptions and catch (or maybe not!) them.  Let's take a look at a typical _Kotlin_
function
```kotlin 
fun createUser(userId: UserId, fname: String, lname: String): User {
...
}
```
This function takes in some args and returns a User.  However this function will likely throw an exception if
- the user exists
- _fname_ and/or _lname_ are invalid
- datastore exceptions
- ...

However there is nothing in the function signature that indicates failure can happen.  This is due to _Kotlin_, _Java_,
etc... being built on top of the _JVM_ and exceptions were designed to be thrown anywhere at anytime!  While this library
will never attempt to change this aspect of the _JVM_ it makes an effort to "document" functions that can return errors.
```kotlin
fun createUser(userId: UserId, fname: String, lname: String): Res<User> {
  ...
}
```
There is a new type here: `Res<User>`  This type indicates the function
will return an error or a value.  Just because `Res<T>` is the _return type_ does not mean the function cannot throw
an exception.  It can if the function implementor does not catch exceptions and convert those to a `Res<Nothing>`.  But
at least the function implementor can indicate in the type signature that an error can occur!

Let's investigate `Res<T>` further.

### Error
There are 3 different types of errors tracked by `Res<T>`

- _Message_ models errors where there is only a reason but no corresponding `Throwable`  Create an instance via

  `Err.message("reason here")`
- _Thrown_ models errors where a cause (`Throwable`) is associated with a reason.  Create an instance via

  `Err.thrown(exception, "reason here")`

- _Context_ models errors with potential many contexts can be associated with a "root" cause.
  A root cause is either a `Message` or `Thrown` (as a type `Either<Message, Thrown>`).  Here's
  an example: Say you're writing a db framework function and it returns an error
  ```kotlin
  val error = Err.thrown(exc, "Unable to retrieve connection")
  ```
  However this error message has no context for the application developer using your amazing function.  What `Context`
  does is give the developer ability to provide context for the db _error_

  ```kotlin
    val error = Err.thrown(exc, "db")
    val yourError = error.context("Unable to query for person")
  ```

### Ok
Values are represented by a generic container of a single value. Create an instance via
  ```kotlin
    val a: Res<Int> = Res.ok(1)
  ```

### Implementation
This particular implementation uses a sealed class hierarchy:
```kotlin 
sealed class Res<out T>
data class Ok<out T> ... : Res<T>()
sealed class Err ... : Res<Nothing>()

data class Message ... : Err()
data class Thrown ... : Err()
data class Context ... : Err()
```
Not all the code is shown because this is an open-source project and you can go look at the actual source.

#### Implementation observations
- This could be implemented using an `Either<L, R>` type and `typealias Res<T> = Either<Err, T>`.  This was not chosen
  as the readability of _Ok_, _Err_, _Message_, _Thrown_, _Context_ is more important than saving some keystrokes.
- The lack of _map_ and _flatMap_ functions is purposely done to encourage the use of the `res { ... }` and
  `suspendRes { ... }` api's.  If you would like to have both api's available here you go
  ```kotlin
    inline fun <T, U> Res<T>.flatMap(block: (T) -> Res<U>): Res<U> = when (this) {
      is Ok -> block(value)
      is Err -> this
    }
    
    inline fun <T, U> Res<T>.map(block: (T) -> U): Res<U> = when (this) {
      is Ok -> Res.ok(block(value))
      is Err -> this
    }
  ```

### Example
Let's take a look at some imperative code (think Java) that contains some amount of error handling:
```kotlin
fun imperative() {
  val request = getApiRequest()
  if (validate(request)) {
    throw IllegalStateException("request is not valid")
  }
  val prepared = prepareRequest(request)
  val entity = try {
    database.update(prepared)
  } catch (exc: Exception) {
    throw IllegalStateException("Unable to save to database", exc)
  }
  try {
    sendConfirmation(entity)
  } catch (exc: Exception) {
    throw IllegalStateException("Unable to send confirmation", exc)
  }
  return entity
}
```
Notice that if there was no error handling (fantasy land) the code would look like
```kotlin
fun fantasy() {
  val request = getApiRequest()
  validate(request)
  val prepared = prepareRequest(request)
  val entity = database.update(prepared)
  sendConfirmation(entity)
  return entity
}
```
We see a difference of 10 lines of code!  Most of that is just language syntax surrounding try/catch.  Do we
really need to type all of the code for error handling?  How many times have developers (maybe even you) not written
error handling code because there's already a "too" much code?!  Let's do better!

We'll use `Res<T>` combined with `res { ... }` api to tame this code along with error handling.
First we modify the signatures of the methods to return `Res<T>`
- `getApiRequest(): Res<?>`
- `validate(...): Res<?>`
- `prepareRequest(...): Res<?>`

However for the purpose of the example let's assume that the
- `database.update(...)`
- `sendConfirmation(entity)`
  calls our 3rd party library functions that we cannot modify.

Next we use `res { ... }`
```kotlin
fun best() = res {
  val request = getApiRequest().value()
  validate(request)
  val prepared = prepareRequest(request).value()
  val entity = catch { database.update(prepared) } value { "Unable to update database!" }
  catch { sendConfirmation(entity) } value { "Could not send confirmation" }
  entity
}
```
This implementation is the exact same number of lines as `fantasy()` function above!  Here's the breakdown
`res { ... }` provides a number of extension functions (see _ResDsl_ for all functions)
- `val request = getApiRequest().value()`  Extracts the value (request in this case) or if there was an error
  (no value to extract) the _res_ block is done and will result in an error
- `val entity = catch { database.update(prepared) } value { "Unable to update database!" }` will be the result of
  `database.update(prepared)` or the _res_ block is done and will result in an error containing the exception and the
  error message _"Unable to update database!"_
- `catch { sendConfirmation(entity) } value { "Could not send confirmation" }` ignores the successful value or the _res_
  block is done and will result in an error containing the exception and the error message _"Could not send confirmation"_
- the result of _res_ is the last expression in the block.  In this case _entity_

#### Control Flow visualization
![foobar](doc-resources/Untitled%20Diagram-Page-1.drawio.png)

The green lines shows the happy path.  The red lines show the error paths.  _outcome_ will be the result of _entity_ if
all happy paths were taken.  Else _outcome_ is the result of one of the errors!

Woohoo!  We've successfully written understandable and concise code!  Our _fantasy_ has been brought to life.

### More visualization
`Res<T>` allows us to visualize any function (at least when called within _res_ or _suspendRes_ blocks) as shown below

![class hierarchy](doc-resources/kata-res-drawio.drawio.png)
