package tech.codingzen.kata.result

import org.junit.Test
import kotlin.test.assertEquals
import kotlin.test.assertIs
import kotlin.test.assertTrue

class ResDslTest {
  @Test
  fun simplest() {
    assertEquals(Res.ok(1), res { 1 })
  }

  @Test
  fun value() {
    assertEquals(Res.ok(1), res { Res.ok(1).value() })
  }

  @Test
  fun valueOr() {
    assertEquals(Res.ok(1), res { Res.ok(1).valueOr(20) })
    assertEquals(Res.ok(20), res { Err.message("nope").valueOr(20) })
  }

  @Test
  fun err() {
    run {
      val msg = "abc"
      assertEquals(Err.message(msg), res {
        err(msg)
        throw RuntimeException("should never be thrown")
      })
    }
    run {
      val msg = "abc"
      val exc = IllegalStateException()
      assertEquals(Err.thrown(exc, msg), res {
        err(exc, msg)
        throw RuntimeException("should never be thrown")
      })
    }
  }

  @Test
  fun catch() {
    run {
      assertEquals(Res.ok(123), res {
        catch { "123".toIntOrNull() ?: throw RuntimeException("never thrown") }.value()
      })
    }
    run {
      val exc = IllegalStateException()
      val res = res {
        val x: Int = catch { "123a".toIntOrNull() ?: throw exc }.value()
        throw RuntimeException("should never be thrown")
      }
      assertIs<Err>(res)
      assertEquals(exc, res.throwable)
    }
    run {
      assertEquals(Res.ok(123), res {
        catch { "123".toIntOrNull() ?: throw RuntimeException("never thrown") } context { "nope" }
      })
    }
    run {
      val msg = "abc"
      val exc = IllegalStateException()
      assertEquals(Err.thrown(exc, msg), res {
        val x: Int = catch { "123a".toIntOrNull() ?: throw exc } context { msg }
        throw RuntimeException("should never be thrown")
      })
    }
    run {
      val msg = "abc"
      val exc = Error()
      assertEquals(Err.thrown(exc, msg), res {
        val x: Int = catch { throw exc } context { msg }
        throw RuntimeException("should never be thrown")
      })
    }
  }

  @Test
  fun uncaught() {
    val exc = RuntimeException("masks the KataErrException")
    val outcome = res {
      try {
        err("masked by this try/catch")
      } catch (t: Throwable) {
        throw exc
      }
    }
    assertTrue(outcome is UncaughtThrown)
    assertEquals(exc, outcome.uncaught)
  }
}