package tech.codingzen.kata.result

import kotlinx.coroutines.runBlocking
import org.junit.Test
import kotlin.test.assertEquals
import kotlin.test.assertIs
import kotlin.test.assertTrue

class SuspendResDslTest {
  @Test
  fun simplest() = runBlocking {
    assertEquals(Res.ok(1), suspendRes { 1 })
  }

  @Test
  fun value() = runBlocking {
    assertEquals(Res.ok(1), suspendRes { Res.ok(1).value() })
  }

  @Test
  fun valueOr() {
    runBlocking { assertEquals(Res.ok(1), suspendRes { Res.ok(1).valueOr(20) }) }
    runBlocking { assertEquals(Res.ok(20), suspendRes { Err.message("nope").valueOr(20) }) }
  }

  @Test
  fun err() {
    runBlocking {
      val msg = "abc"
      assertEquals(Err.message(msg), suspendRes {
        err(msg)
        throw RuntimeException("should never be thrown")
      })
    }
    runBlocking {
      val msg = "abc"
      val exc = IllegalStateException()
      assertEquals(Err.thrown(exc, msg), suspendRes {
        err(exc, msg)
        throw RuntimeException("should never be thrown")
      })
    }
  }

  @Test
  fun catch() {
    runBlocking {
      assertEquals(Res.ok(123), suspendRes {
        catch { "123".toIntOrNull() ?: throw RuntimeException("never thrown") }.value()
      })
    }
    runBlocking {
      val exc = IllegalStateException()
      val res = suspendRes {
        val x: Int = catch { "123a".toIntOrNull() ?: throw exc }.value()
        throw RuntimeException("should never be thrown")
      }
      assertIs<Err>(res)
      assertEquals(exc, res.throwable)
    }
    runBlocking {
      assertEquals(Res.ok(123), suspendRes {
        catch { "123".toIntOrNull() ?: throw RuntimeException("never thrown") } context  { "nope" }
      })
    }
    runBlocking {
      val msg = "abc"
      val exc = IllegalStateException()
      assertEquals(Err.thrown(exc, msg), suspendRes {
        val x: Int = catch { "123a".toIntOrNull() ?: throw exc } context  { msg }
        throw RuntimeException("should never be thrown")
      })
    }
  }

  @Test
  fun uncaught() = runBlocking {
    val exc = RuntimeException("masks the KataErrException")
    val outcome = suspendRes { 
      try {
        err("masked by this try/catch")
      } catch (t: Throwable) {
        throw exc
      }
    }
    assertTrue(outcome is UncaughtThrown)
    assertEquals(exc, outcome.uncaught)
  }
}