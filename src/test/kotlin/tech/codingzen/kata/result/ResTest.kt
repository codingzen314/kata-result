package tech.codingzen.kata.result

import org.junit.Test
import tech.codingzen.kata.list.KataList.Companion.katalist
import kotlin.test.assertEquals
import kotlin.test.assertIs
import kotlin.test.assertNull

class ResTest {
  @Test
  fun message() {
    val errmsg = "foo"
    val err = Err.message(errmsg)
    assertIs<Message>(err)
    assertEquals(errmsg, err.context)
    assertNull(err.throwable)
    assertEquals(katalist(errmsg) to null, err.parts)
  }

  @Test
  fun thrown() {
    val errmsg = "foo"
    val exc = RuntimeException("bar")
    val err = Err.thrown(exc, errmsg)
    assertIs<Thrown>(err)
    assertEquals(errmsg, err.context)
    assertEquals(exc, err.throwable)
    assertEquals(katalist(errmsg) to exc, err.parts)
  }

  @Test
  fun linked() {
    val errmsg0 = "foo"
    val errmsg1 = "baz"
    val errmsg2 = "bar"
    val exc = RuntimeException("bar")
    val err = Err.thrown(exc, errmsg0).context(errmsg1).context(errmsg2)
    assertIs<Context>(err)
    assertEquals(errmsg2, err.context)
    assertEquals(exc, err.throwable)
    assertEquals(katalist(errmsg2, errmsg1, errmsg0) to exc, err.parts)
  }

  @Test
  fun res_link() {
    run {
      val res = Res.ok(1)
      assertIs<Res<Int>>(res)
      val linked = res.context { "nope" }
      assertIs<Res<Int>>(linked)
    }
    run {
      val errmsg0 = "foo"
      val errmsg1 = "baz"
      val res = (Err.message(errmsg0) as Res<Int>).context { errmsg1 }
      assertIs<Err>(res)
    }
  }

  @Test
  fun caught() {
    val exc = RuntimeException("foo")
    val errmsg = "bar"
    val catchRes = CatchRes.caught(exc)
    assertIs<Caught>(catchRes)
    val res = catchRes.toRes<Int> { errmsg }
    assertIs<Thrown>(res)
    val err = res as Thrown
    assertEquals(errmsg, err.context)
    assertEquals(exc, err.throwable)
  }

  @Test
  fun caughtError() {
    val error = Error("test")
    val errmsg = "test message"
    val catchRes = CatchRes.caught(error)
    assertIs<Caught>(catchRes)
    val res = catchRes.toRes<Int> { errmsg }
    assertIs<Thrown>(res)
    val err = res as Thrown
    assertEquals(errmsg, err.context)
    assertEquals(error, err.throwable)
  }

  @Test
  fun value() {
    val value = 1
    val catchRes = CatchRes.value(value)
    assertIs<Value<Int>>(catchRes)
    val res = catchRes.toRes { "nope" }
    assertIs<Ok<Int>>(res)
    val container = res as Ok
    assertEquals(value, container.value)
  }
}