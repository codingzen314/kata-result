package tech.codingzen.kata.result

import tech.codingzen.kata.list.KataList
import tech.codingzen.kata.list.KataList.Companion.katalist
import kotlin.coroutines.intrinsics.COROUTINE_SUSPENDED
import kotlin.coroutines.intrinsics.suspendCoroutineUninterceptedOrReturn
import kotlin.coroutines.resumeWithException

/**
 * Represents success as [Ok] or failure as [Err]
 *
 * @param T value type contained in [Ok]
 */
sealed class Res<out T> {
  companion object
}


/**
 * Represents success
 *
 * @param T value type
 * @param [value] successful value
 */
data class Ok<out T> internal constructor(val value: T) : Res<T>()

/**
 * Represents failure.  Root of the error hierarchy
 */
sealed class Err protected constructor() : Res<Nothing>() {
  companion object
}

/**
 * Represents an error containing only a message
 */
data class Message internal constructor(val msg: String) : Err()

/**
 * Represents an error containing the exceptional cause and message
 */
data class Thrown internal constructor(val thrown: Throwable, val msg: String) : Err()

/**
 * Represents an error containing the exceptional cause
 */
data class UncaughtThrown @PublishedApi internal constructor(val uncaught: Throwable): Err() {
  val msg = "__Uncaught Exception__"
}

/**
 * Represents an error containing multiple messages and a nullable exceptional cause
 */
data class Context @PublishedApi internal constructor(val contexts: KataList<String>, val thrown: Throwable? = null) : Err()

/**
 * @param T value type
 * @param [value] successful value
 * @return Ok containing [value]
 */
fun <T> Res.Companion.ok(value: T): Res<T> = Ok(value)

/**
 * @param [message] error message
 * @return Err containing only this error message but no exceptional cause
 */
fun Err.Companion.message(message: String): Err = Message(message)

/**
 * @param [thrown] exceptional cause
 * @param [message] error message
 * @return Err containing the exceptional cause and message
 */
fun Err.Companion.thrown(thrown: Throwable, message: String): Err = Thrown(thrown, message)

/**
 * @receiver the Err instance
 * @return the most recent error message
 */
val Err.context: String
  get() = when (this) {
    is Message -> msg
    is Thrown -> msg
    is Context -> contexts.head
    is UncaughtThrown -> msg
  }

/**
 * @receiver the Err instance
 * @return the exceptional cause as a Throwable or `null` if there is no exceptional cause
 */
val Err.throwable: Throwable?
  get() = when (this) {
    is Message -> null
    is Thrown -> thrown
    is Context -> thrown
    is UncaughtThrown -> uncaught
  }

/**
 * @receiver the Err instance
 * @return a Pair containing all the error message(s) and the exceptional cause (should it exist)
 */
val Err.parts: Pair<KataList<String>, Throwable?>
  get() = when (this) {
    is Message -> katalist(msg) to null
    is Thrown -> katalist(msg) to thrown
    is Context -> contexts to thrown
    is UncaughtThrown -> katalist(msg) to uncaught
  }

/**
 * @receiver the Err instance that will have context added to it
 * @param [message] add an error message as context to [this] existing Err
 * @return a new [Err] instance containing the new contextual error message
 */
fun Err.context(message: String): Err = when (this) {
  is Message -> Context(katalist(msg) + message)
  is Thrown -> Context(katalist(msg) + message, thrown)
  is Context -> copy(contexts = contexts + message)
  is UncaughtThrown -> Context(katalist(msg) + message, uncaught)
}

/**
 * @receiver Res that will have context added to it
 * @param [T] value type
 * @param [block] produces the context to be added to [this] Res
 * @return [this] if this is [Ok] or a new [Err] that has the context produced by [block] added to it
 */
inline fun <T> Res<T>.context(crossinline block: () -> String): Res<T> = when (this) {
  is Ok -> this
  is Err -> context(block())
}

/**
 * Represents the result from a `catch { .. }` statement executed inside a [res] block
 */
sealed class CatchRes<out T> protected constructor() {
  companion object
}

/**
 * Represents the caught exceptional cause
 *
 * @param [thrown] caught exceptional cause
 */
data class Caught internal constructor(val thrown: Throwable) : CatchRes<Nothing>()

/**
 * Represents success
 *
 * @param T value type
 * @param [value] successful value
 */
data class Value<out T> internal constructor(val value: T) : CatchRes<T>()

/**
 * Smart constructor for [Caught]
 *
 * @param [thrown] exceptional cause
 * @return instance of [CatchRes] containing the exceptional cause
 */
fun CatchRes.Companion.caught(thrown: Throwable): CatchRes<Nothing> = Caught(thrown)

/**
 * Smart constructor for [Value]
 *
 * @param T value type
 * @param [value] successful value
 * @return instance of [CatchRes] containing the successful value
 */
fun <T> CatchRes.Companion.value(value: T): CatchRes<T> = Value(value)

/**
 * @receiver CatchRes instance to convert to [Res]
 * @param T value type
 * @param [messageSupplier] supplies an error message
 * @return instance of Res that is [Ok] if [this] is [Value], else [Err] if [this] is [Caught]
 */
fun <T> CatchRes<T>.toRes(messageSupplier: () -> String): Res<T> = when (this) {
  is Caught -> Err.thrown(thrown, messageSupplier())
  is Value -> Res.ok(value)
}

/**
 * DO NOT USE THIS CLASS!  Only exposed due to inline functions.
 *
 * Used internally to control execution within a [res] block.
 */
@PublishedApi
internal class ErrException internal constructor(val err: Err) : Exception(null as String?) {
  override fun fillInStackTrace(): Throwable = this
}

/**
 * Represents a DSL for error handling in kotlin
 */
class ResDsl internal constructor() {
  @PublishedApi
  internal companion object {
    val instance = ResDsl()
  }

  /**
   * @receiver Res that may hold a value that needs to be retrieved
   * @param T value type
   * @return the value if [this] is [Ok], else exit the [res] block
   */
  fun <T> Res<T>.value(): T = when (this) {
    is Ok -> value
    is Err -> throw ErrException(this)
  }

  /**
   * @receiver Res that may hold a value that needs to be retrieved
   * @param T value type
   * @param [default] default value
   * @return the value if [this] is [Ok], else [default]
   */
  fun <T> Res<T>.valueOr(default: T): T = when (this) {
    is Ok -> value
    is Err -> default
  }

  /**
   * Exit the [res] block with an error containing only a message
   * @param [error] error message
   */
  fun err(message: String): Nothing = Err.message(message).value()

  /**
   * Exit the [res] block with an error containing the exceptional cause and a message
   * @param [thrown] exceptional cause
   * @param [error] error message
   */
  fun err(thrown: Throwable, message: String): Nothing = Err.thrown(thrown, message).value()

  /**
   * Catches all exceptions throw from a block of code
   * @param T value type
   * @param [block] code that may throw an exception or produces a value
   * @return instance of [Value] if the execution of [block] produced a value, else an instance of [Caught] containing
   * the exceptional cause
   */
  inline fun <T> catch(block: () -> T): CatchRes<T> =
    try {
      CatchRes.value(block())
    } catch (thrown: Throwable) {
      CatchRes.caught(thrown)
    }

  /**
   * @receiver [CatchRes] that may hold a value or an exceptional cause
   * @param T value type
   * @param [block] error message supplier
   * @return the value if [this] is [Value], else exit the [res] block with an [Err] containing the exceptional cause
   * held in [this] and error message produced by [block]
   */
  infix fun <T> CatchRes<T>.context(block: () -> String): T = toRes(block).value()

  /**
   * @receiver [CatchRes] that may hold a value or an exception cause
   * @param T value type
   * @return the value if [this] is [Value], else exit the [res] block with an [Err] containing the exceptional cause
   * held in [this]
   */
  fun <T> CatchRes<T>.value(): T = toRes {
    "Consider providing a message via `catch { ... } context { \"your meaningful message here\" }`"
  }.value()

  /**
   * @receiver [Res] that may hold a value or an [Err]
   * @return Unit if [this] is an [Ok], else exit the [res] block
   */
  fun Res<*>.execute(): Unit {
    value()
  }
}

/**
 * @param T value type
 * @param [block] block that produces an instance of T or exits early with an [Err]
 * @return instance of [Ok] if [block] successfully produced a value, else the [Err]
 */
inline fun <T> res(block: ResDsl.() -> T): Res<T> =
  try {
    Res.ok(ResDsl.instance.block())
  } catch (exc: ErrException) {
    exc.err
  } catch (exc: Throwable) {
    UncaughtThrown(exc)
  }

/**
 * Represents a DSL for error handling in kotlin.  Similar to [ResDsl] but for suspending functions and coroutines
 */
class SuspendResDsl internal constructor() {
  @PublishedApi
  internal companion object {
    val instance = SuspendResDsl()
  }

  /**
   * @receiver Res that may hold a value that needs to be retrieved
   * @param T value type
   * @return the value if [this] is [Ok], else exit the [suspendRes ] block
   */
  suspend fun <T> Res<T>.value(): T =
    suspendCoroutineUninterceptedOrReturn { cont ->
      when (this) {
        is Ok -> value
        is Err -> COROUTINE_SUSPENDED.apply {
          cont.resumeWithException(ErrException(this@value));
        }
      }
    }

  /**
   * @receiver Res that may hold a value that needs to be retrieved
   * @param T value type
   * @param [default] default value
   * @return the value if [this] is [Ok], else [default]
   */
  fun <T> Res<T>.valueOr(default: T): T = when (this) {
    is Ok -> value
    is Err -> default
  }

  /**
   * Exit the [suspendRes] block with an error containing only a message
   * @param [error] error message
   */
  suspend fun err(message: String): Nothing = Err.message(message).value()

  /**
   * Exit the [suspendRes] block with an error containing the exceptional cause and a message
   * @param [thrown] exceptional cause
   * @param [error] error message
   */
  suspend fun err(thrown: Throwable, message: String): Nothing = Err.thrown(thrown, message).value()

  /**
   * Catches all exceptions throw from a block of code
   * @param T value type
   * @param [block] code that may throw an exception or produces a value
   * @return instance of [Value] if the execution of [block] produced a value, else an instance of [Caught] containing
   * the exceptional cause
   */
  suspend inline fun <T> catch(crossinline block: suspend () -> T): CatchRes<T> =
    try {
      val value = block()
      CatchRes.value(value)
    } catch (thrown: Throwable) {
      CatchRes.caught(thrown)
    }

  /**
   * @receiver [CatchRes] that may hold a value or an exceptional cause
   * @param T value type
   * @param [block] error message supplier
   * @return the value if [this] is [Value], else exit the [suspendRes] block with an [Err] containing the exceptional cause
   * held in [this] and error message produced by [block]
   */
  suspend infix fun <T> CatchRes<T>.context(block: () -> String): T = toRes(block).value()

  /**
   * @receiver [CatchRes] that may hold a value or an exception cause
   * @param T value type
   * @return the value if [this] is [Value], else exit the [suspendRes] block with an [Err] containing the exceptional cause
   * held in [this]
   */
  suspend fun <T> CatchRes<T>.value(): T = toRes {
    "Consider providing a message via `catch { ... } valueOrErr { \"your meaningful message here\" }`"
  }.value()

  /**
   * @receiver [Res] that may hold a value or an [Err]
   * @return Unit if [this] is an [Ok], else exit the [suspendRes] block
   */
  suspend fun Res<*>.execute(): Unit {
    value()
  }
}

/**
 * @param T value type
 * @param [block] block that produces an instance of T or exits early with an [Err]
 * @return instance of [Ok] if [block] successfully produced a value, else the [Err]
 */
suspend inline fun <T> suspendRes(crossinline block: suspend SuspendResDsl.() -> T): Res<T> =
  try {
    Res.ok(SuspendResDsl.instance.block())
  } catch (exc: ErrException) {
    exc.err
  } catch (exc: Throwable) {
    UncaughtThrown(exc)
  }